// Student mark program
const studentMark = 50;

if (studentMark < 50) {
    console.log(`your mark is ${studentMark} your failed`);
}else{
    console.log(`your mark is ${studentMark} your passed`);
}

// OTT platform program
const yourAge = 21;

if (yourAge >= 18) {
    console.log("Your eligible enter the ott platform");
}else {
    console.log(`your age is below 18 then you not eligible entering`)
}


// square program
function square(number) {
    return number * number;
}

const num1 = 5;
const num2 = 3;
const squareOfNum1 = square(num1);
const squareOfNum2 = square(num2);

console.log("Square of", num1, "is", squareOfNum1); 
console.log("Square of", num2, "is", squareOfNum2);
